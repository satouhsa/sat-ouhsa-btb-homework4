import { Textarea } from 'native-base'

import { SafeAreaView,ScrollView,Image,TouchableOpacity } from 'react-native'
import { View, KeyboardAvoidingView, TextInput, StyleSheet, Text, Platform, TouchableWithoutFeedback, Keyboard  } from 'react-native';
import { Card, ListItem, Button, Icon } from 'react-native-elements'
import ImagePicker from 'react-native-image-picker';



import React, { Component } from 'react'


export class NewStrory extends Component {


  constructor(props) {
    super(props);
    this.state = {
      resourcePath: {},
      des: '',
    };
  }

  selectFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose file from Custom Option',
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, res => {
      console.log('Response = ', res);

      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        console.log('ImagePicker Error: ', res.error);
      } else if (res.customButton) {
        console.log('User tapped custom button: ', res.customButton);
        alert(res.customButton);
      } else {
        let source = res;
        this.setState({
          resourcePath: source,
        });
      }
    });
  };
  render() {

    const {navigate} = this.props.navigation;
    let {btnPost} = styles;
    return (

      <SafeAreaView>
      <ScrollView>

          <View>

              <View>

                  <Card>

                                <Image
                          source={{uri: this.state.resourcePath.uri}}
                          style={{
                            backgroundColor: '#eee',
                            marginTop: 0,
                            height: 250,
                            width: 350,
                            bottom: 0,
                          }}
                        />
                    
                     

                      

                  </Card>

                  <View style={{marginTop:25,marginLeft:20}}>
                             <TouchableOpacity onPress={this.selectFile}>
                                      <Text style={{fontSize:18,fontWeight:'bold',color:'blue'}} >Select File</Text>
                             </TouchableOpacity>
                  </View>

                  <View style={{marginTop:30}}>
                             <KeyboardAvoidingView 
                                  behavior={Platform.OS == "ios" ? "padding" : "height"}
                                  style={styles.container}
                                  >
                                  <TouchableWithoutFeedback onPress={Keyboard.dismiss} style={{height:30}}>
                                      
                                      <ScrollView>

                                      <Textarea 
                                              value={this.state.des}
                                              onChangeText={des => this.setState({des})}
                                              rowSpan={5}
                                              placeholder="what's your new post here...."
                                              style={{marginLeft:20,height:240,width:377,borderColor:"white",borderWidth:3}}
                                          />

                                      </ScrollView>
                                         
                                      
                                     
                                  </TouchableWithoutFeedback>
                              </KeyboardAvoidingView>
                          
                  </View>

                  <View style={{marginLeft:30,marginTop:20}}>

                          <View style={{flex: 1, flexDirection: 'row'}}>
                              <View>
                                      <TouchableOpacity>
                                                  <Text style={{fontSize:16,fontWeight:'bold',color:"#3caea3"}}>Draft</Text>
                                      </TouchableOpacity>
                              </View>
                              <View>
                                  <TouchableOpacity>
                                              <Text style={{fontSize:16,fontWeight:'bold',color:"#3caea3"}}>      Publich</Text>
                                  </TouchableOpacity>

                              </View>
                              
                          </View>


                  </View>

              </View>

          </View>

      </ScrollView>
</SafeAreaView>

    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
   
  },
  inner: {
    padding: 24,
    flex: 1,
    justifyContent: "space-around"
  },
  header: {
    fontSize: 36,
    marginBottom: 48
  },
  textInput: {
    height: 40,
    borderColor: "#000000",
    borderBottomWidth: 1,
    marginBottom: 36
  },
  btnContainer: {
    backgroundColor: "white",
    marginTop: 12
  }
});


export default NewStrory;



