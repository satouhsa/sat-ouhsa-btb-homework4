import React from 'react'
import { Container, Header, Content,  CardItem, Body } from 'native-base';
import {Card} from 'react-native-elements';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar,Image,TouchableOpacity } from 'react-native';










const DATA=[

    {
        title:'Explainer: Why AP hasnt called Georgias close race',
        description:'WASHINGTON: A razor-thin margin and ongoing vote count are what’s making the Georgia contest between President Donald Trump and Joe Biden too early to call Votes are still being counted across the state, though many from counties where Biden was in the lead',
        img: "https://cna-sg-res.cloudinary.com/image/upload/q_auto,f_auto/image/13482758/16x9/991/557/26aab0353840a996eb3c97f7e731b8c9/XE/the-statue-of-former-u-s--president-george-washington-stands-across-the-new-york-stock-exchange--nyse--following-election-day-in-manhattan--new-york-city-3.jpg",
        time:'4 min read ',
        name:'Rose Billy',

    },
    {
        title:'The Top 10 Best Antivirus Providers For Mac (2020)',
        description:'Are you looking for antivirus software for your Apple computer but don’t know who to trust? Or are you unsure if your antivirus is the right choice? These days, having trusted antivirus software is an important part of life. With recent media concerns over personal data security, and numerous harmful viruses effecting thousands of computer owners worldwide, it’s important you',
        img: "https://cna-sg-res.cloudinary.com/image/upload/q_auto,f_auto/image/13484184/16x9/991/557/4fc153de1d728b8bbc6c48d1bc49a303/pX/election-2020-protests-philadelphia-92925-jpg-1604694371.jpg",
        time:'5 min read',
        name:'Bull Chin',

    },
    {
        title:'Some nightlife businesses allowed to reopen with COVID-19 safety measures under pilot programme',
        description:'SINGAPORE: A "limited number" of nightlife establishments will be allowed to reopen with COVID-19 safety measures in place under a pilot programme, the Ministry of Trade and Industry (MTI) and the Ministry of Home Affairs (MHA) said on Friday (Nov 6)The Government has been in close consultation with the nightlife industry to understand their concerns and has agreed to allow a limited number of nightlife',
        img: "https://24liveblog.tradingfront.cn/event/2646498779702104530/20201106180313_527862.webp",
        time:'30 min read ',
        name:'Rose Noona',

    },
    {
        title:'Workers who have recovered from COVID-19 exempted from rostered routine testing: MOH, MOM',
        description:'SINGAPORE: All workers who have recovered from COVID-19 are now exempted from rostered routine testing, said the Ministry of Health (MOH) and Ministry of Manpower (MOM) in a joint press release on Friday (Nov 6)The move comes after a review of the latest scientific evidence suggested that these workers are at "low risk" of re-infection, said the ministries.',
        img: "https://24liveblog.tradingfront.cn/event/2646498779702104530/20201106185253_834931.jpeg",
        time:'23 min read ',
        name:' Vi Konisa',

    },
    {
        title:'President Donald Trump’s Childhood Home Up For Auction Again',
        description:'President Donald Trump’s childhood home in Queens, New York, is up for auction again with an undisclosed reserve price. Bidding launched today and will be open until Nov. 14.The Jamaica Estates home listed earlier this year for $2.9 million, well over the median single-family home sale price of $1.1856 for the area, according to Zillow. After it listed, it swiftly went to auction with Paramount Realty USA.',
        img: "https://24liveblog.tradingfront.cn/event/2646498779702104530/20201106195642_180289.webp",
        time:'45 min read',
        name:'Jisoo Ben',

    },
    {
        title:'UKs Johnson says he is confident in US election checks and balances',
        description:'LONDON: British Prime Minister Boris Johnson said he had confidence in the US election process, after US President Donald Trump made unsubstantiated claims of fraud following Tuesdays presidential vote.',
        img: "https://24liveblog.tradingfront.cn/event/2646498779702104530/20201106195642_605769.webp",
        time:'36 min read',
        name:'Bille Eille',

    },
    {
        title:'US state of Georgia says it will recount razor-thin vote',
        description:'WASHINGTON: Georgias Secretary of State Brad Raffensperger on Friday (Nov 6) said he expects a recount due to the small margin for the presidential election in the battleground state, where Democrat Joe Biden has a small lead over President Donald Trump of more than 1,000 votes, with 4,169 left to count.',
        img: "https://24liveblog.tradingfront.cn/event/2646498779702104530/20201106202244_289855.webp",
        time:'56 min read',
        name:'Sat ouhsa',

    },
    {
        title:'Biden overtakes Trump in Georgia vote count',
        description:'ATLANTA, Georgia: Democrat Joe Biden is now leading President Donald Trump in the battleground state of Georgia By Friday (Nov 6) morning, Biden overtook Trump in the number of ballots counted in the battleground, a must-win state for Trump that has long been a Republican stronghold. Biden now has a 917-vote advantage.',
        img:"https://cna-sg-res.cloudinary.com/image/upload/q_auto,f_auto/image/13476694/16x9/991/557/ac882bcca39fc8d3186e1b6c36c5c8d4/On/combination-picture-of-democratic-u-s--presidential-nominee-joe-biden-and-u-s--president-donald-trump-speaking-about-the-early-results-of-the-2020-u-s--presidential-election--u-s--1.jpg",
        time:'67 min read ',
        name:'Ny Jooni',

    },
    {
        title:'Top Democrat Pelosi calls Biden president-elect',
        description:'WASHINGTON: House Speaker Nancy Pelosi, the top Democrat in Congress, on Friday (Nov 6) called Joe Biden the "president-elect" of the United States after he pulled ahead in key election results. Pelosi told reporters after Biden overtook President Donald Trump in the potentially decisive state of Pennsylvania',
        img: "https://cna-sg-res.cloudinary.com/image/upload/q_auto,f_auto/image/13463160/16x9/991/557/fda8b0e86f8eb9f7aa3e42ef4e9e04eb/qF/file-photo--the-logo-of-ant-group--an-affiliate-of-alibaba--at-its-headquarters-in-hangzhou--china-1.jpg",
        time:'78 min read',
        name:' Jonh nija',

    },
    {
        title:'Video games, long runs and Riesling: How Wall Streeters calmed election nerves',
        description:'As U.S. election results started trickling in late Tuesday evening, hedge fund manager Eric Jackson stress-ate so much of his kids Halloween candy that he felt compelled to buy a blood sugar monitor the next day.',
        img: "https://cna-sg-res.cloudinary.com/image/upload/q_auto,f_auto/image/13467090/16x9/991/557/2f21cc46cf24af4d0d8d5b417355e65e/qr/file-photo--2020-u-s--presidential-election-in-new-york-1.jpg",
        time:'90 min read ',
        name:'Ben jipo',

    },

]
const Item = ({ title,time,name,img }) => (

    <View style={{flex: 1, flexDirection: 'row',marginVertical:20,marginHorizontal:1}}>
        <View style={{flex:3}}>
            <Text style={{fontWeight:'bold'}}>{title}</Text>
            <Text style={{marginTop:10,marginBottom:8}}>{name}</Text>
            <Text>{time}</Text>
        </View>
        <View style={{flex:2}}>

        <Image source={{uri: img}} style={{width: 190, height: 100}}>
    
        </Image>

        </View>
  
      </View>

  );

const Homepage = ({navigation}) => {
    const renderItem = ({item}) => (
        <View style={{borderColor:'blue'}}>
         
          <TouchableOpacity onPress={()=> navigation.navigate('Show', item)} >
  
          <Item title={item.title}  
            name={item.name}
            time={item.time}
            img={item.img}
            />
  
          </TouchableOpacity>
  
            
  
        </View>
         
         
        );
       
    return (
        <Container>
        {/* <Header style={{backgroundColor:'#3caea3'}}><Text style={{fontSize:20,fontWeight:'bold',marginTop:15,color:"white"}}>Home Screen</Text></Header> */}
        <Content>

            <Text style={{fontSize:18,fontWeight:'bold',marginTop:20,marginLeft:20,marginBottom:20}}>Your Daily Read</Text>
          <Card style={{borderColor:'blue'}} >
            <CardItem  >
            <SafeAreaView style={styles.container}>
                <FlatList
               
                data={DATA}
                renderItem={renderItem}
                
                //   keyExtractor={item => item.time}
                />
            </SafeAreaView>
            </CardItem>
           
         </Card>
        </Content>
      </Container>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: StatusBar.currentHeight || 0,
   
    },
    item: {
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
      borderColor:'blue'
    },
    title: {
      fontSize: 32,
    },
  });

export default Homepage
