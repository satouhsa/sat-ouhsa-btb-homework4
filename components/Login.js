import React, {Component} from 'react';

import {Text, TextInput, View, Button, StyleSheet, Alert} from 'react-native';
import {Container, Header, Content, Card, CardItem, Body} from 'native-base';
import {
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  onLogin() {
    const {username, password} = this.state;

    // Alert.alert('Credentials', `${username} + ${password}`);
    Alert.alert('Login Successfully');
  }
  render() {
    return (
      <Container>
        {/* <Header style={{backgroundColor:'#3caea3'}}><Text style={{fontSize:20,fontWeight:'bold',marginTop:15,color:"white"}}>Login Screen</Text></Header> */}
        <Content>
          <View style={styles.container}>
            <Text
              style={{
                fontSize: 30,
                fontWeight: 'bold',
                color: 'blue',
                marginBottom: 50,
              }}>
              Hey App
            </Text>

            <KeyboardAvoidingView
              behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
              <TouchableWithoutFeedback
                onPress={Keyboard.dismiss}
                style={{height: 30}}>
                <TextInput
                  value={this.state.username}
                  onChangeText={(username) => this.setState({username})}
                  placeholder={'Username...'}
                  style={styles.input}
                />

                
              </TouchableWithoutFeedback>
            </KeyboardAvoidingView>


            <KeyboardAvoidingView
              behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
              <TouchableWithoutFeedback
                onPress={Keyboard.dismiss}
                style={{height: 30}}>
                    <TextInput
              value={this.state.password}
              onChangeText={(password) => this.setState({password})}
              placeholder={'Password...'}
              secureTextEntry={true}
              style={styles.input}
            />

                
              </TouchableWithoutFeedback>
            </KeyboardAvoidingView>


            

            <Button
              title={'Login'}
              style={styles.input}
              // onPress={this.onLogin.bind(this)}
              onPress={() => this.props.navigation.navigate('HomeScreen')}
            />
            {/* <Button title={'Login'}
                    style={styles.button}
                    onPress={this.onLogin.bind(this)}></Button> */}
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',

    marginTop: 200,
  },
  input: {
    width: 330,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 10,
    borderRadius: 30,
    backgroundColor: 'white',
  },
  button: {
    width: 330,
    height: 44,
    padding: 10,
    borderWidth: 1,
    backgroundColor: 'blue',
  },
});

export default Login;
