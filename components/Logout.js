import React from 'react'
import { View, Text ,Button} from 'react-native'

const Logout = ({navigation}) => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
      
      <Button title="Go to Home" onPress={() => navigation.navigate('Login')} />
     
    </View>
    )
}

export default Logout
