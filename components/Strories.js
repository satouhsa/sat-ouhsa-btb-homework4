import React from 'react'
import { View, Text } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Draft from './Draft';
import Public from './Public';
import Unlisted from './Unlisted';
const MaterialTopTabs = createMaterialTopTabNavigator();

const Strories = () => {
    return (
       
        <MaterialTopTabs.Navigator>
        <MaterialTopTabs.Screen
          name="Drafts"
          component={Draft}
          
        />
        <MaterialTopTabs.Screen name="Public" component={Public} />
        <MaterialTopTabs.Screen name="Unlisted" component={Unlisted} />
      </MaterialTopTabs.Navigator>
    )
}

export default Strories
