import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  CardItem,
} from 'native-base';
import {Card} from 'react-native-elements';
import {Linking} from 'react-native';

const Show = ({navigation, route}) => {
  const {title, description, img, time, name} = route.params;

  return (
    <SafeAreaView>
      <View>
        <Card>
          <Text style={{fontWeight: 'bold', marginBottom: 20, fontSize: 15}}>
            {title}{' '}
          </Text>
          <Text style={{marginBottom: 10}}>{name}</Text>
          <Text style={{fontSize: 12}}>{time}</Text>
          <CardItem cardBody>
            <Image
              source={{uri: img}}
              style={{
                backgroundColor: '#eee',
                marginTop: 30,
                height: 200,
                width: null,
                flex: 1,
              }}
            />
          </CardItem>
          <Text style={{marginTop: 25}}>{description}</Text>

          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 2, marginTop: 30}}>
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL('https://www.facebook.com/');
                }}>
                <Image
                  source={{
                    uri:
                      'https://icons-for-free.com/iconfiles/png/512/facebook+logo+logo+website+icon-1320190502625926346.png',
                  }}
                  style={{width: 80, height: 80, backgroundColor: 'white'}}
                />
              </TouchableOpacity>
            </View>
            <View style={{flex: 2, marginTop: 30}}>
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL('https://www.google.com/');
                }}>
                <Image
                  source={{
                    uri:
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSCrajulrr0IvNayJWIP45UodNJmxjQJOoBNw&usqp=CAU',
                  }}
                  style={{width: 80, height: 80}}
                />
              </TouchableOpacity>
            </View>
            <View style={{flex: 2, marginTop: 30}}>
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL('https://telegram.org/');
                }}>
                <Image
                  source={{
                    uri:
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQkW37owSLL-0MuuA9WVASYTrxT8QKBAX_Hfw&usqp=CAU',
                  }}
                  style={{width: 80, height: 80}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </Card>
      </View>
    </SafeAreaView>
  );
};

export default Show;
