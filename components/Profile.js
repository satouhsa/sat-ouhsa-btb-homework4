import React from 'react'
import { View, Text,Image } from 'react-native'

const Profile = () => {
    return (
        <View style={{marginTop:20,marginLeft:30}}>
            
            <Image source={{uri:'https://static01.nyt.com/images/2020/03/15/magazine/15mag-billie-03/15mag-billie-03-mediumSquareAt3X-v3.jpg'}}
                style={{width:150,height:150,borderRadius:90}}
            />

            <Text style={{marginTop:30}}>Ouhsajoly Megan</Text>
        </View>
    )
}

export default Profile
