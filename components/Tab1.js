import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Card, ListItem, Button, Icon} from 'react-native-elements';

import React, {Component} from 'react';

export class Tab1 extends Component {
  state = {
    toggle: true,
    toggleone: true,
    toggletwo: true,
    togglethree: true,
    togglefour: true,
    togglefive: true,
  };

  onPress() {
    const Sate = !this.state.toggle;
    this.setState({toggle: Sate});
  }
  onPressone() {
    const news = !this.state.toggleone;
    this.setState({toggleone: news});
  }
  onPresstwo() {
    const newSate = !this.state.toggletwo;
    this.setState({toggletwo: newSate});
  }
  onPressthree() {
    const thesate = !this.state.togglethree;
    this.setState({togglethree: thesate});
  }
  onPressfour() {
    const thenew = !this.state.togglefour;
    this.setState({togglefour: thenew});
  }
  onPressfive() {
    const netheSate = !this.state.togglefive;
    this.setState({togglefive: netheSate});
  }

  render() {
    const {
      toggle,
      toggleone,
      toggletwo,
      togglethree,
      togglefour,
      togglefive,
    } = this.state;

    const textValue = toggle ? 'Follow' : 'Following';
    const buttonBg = toggle ? 'white' : 'dodgerblue';
    const textcolor = toggle ? 'black' : 'white';

    // const {toggleone} =this.state;

    const textValueone = toggleone ? 'Follow' : 'Following';
    const buttonBgone = toggleone ? 'white' : 'dodgerblue';
    const textcolorone = toggleone ? 'black' : 'white';

    // const {toggletwo} =this.state;

    const textValuetwo = toggletwo ? 'Follow' : 'Following';
    const buttonBgtwo = toggletwo ? 'white' : 'dodgerblue';
    const textcolortwo = toggletwo ? 'black' : 'white';

    // const {togglethree} =this.state;

    const textValuethree = togglethree ? 'Follow' : 'Following';
    const buttonBgthree = togglethree ? 'white' : 'dodgerblue';
    const textcolorthree = togglethree ? 'black' : 'white';

    // const {togglefour} =this.state;

    const textValuefour = togglefour ? 'Follow' : 'Following';
    const buttonBgfour = togglefour ? 'white' : 'dodgerblue';
    const textcolorfour = togglefour ? 'black' : 'white';

    // const {togglefive} =this.state;

    const textValuefive = togglefive ? 'Follow' : 'Following';
    const buttonBgfive = togglefive ? 'white' : 'dodgerblue';
    const textcolorfive = togglefive ? 'black' : 'white';
    return (
      <SafeAreaView>
        <ScrollView>
          <View>
            <Card>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 2}}>
                  <Text
                    style={{fontSize: 18, fontWeight: 'bold', marginTop: 10}}>
                    Art
                  </Text>
                </View>
                <View>
                  <View style={{flex: 2}}>
                    <TouchableOpacity
                      onPress={() => this.onPress()}
                      style={{
                        margin: 10,
                        flex: 1,
                        height: 45,
                        width: 80,
                        backgroundColor: buttonBg,
                        borderColor: 'blue',
                        borderWidth: 1,
                      }}>
                      <Text
                        style={{
                          color: textcolor,
                          fontSize: 16,
                          textAlign: 'center',
                          marginTop: 10,
                        }}>
                        {textValue}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 2}}>
                  <Text
                    style={{fontSize: 18, fontWeight: 'bold', marginTop: 10}}>
                    Book
                  </Text>
                </View>
                <View>
                  <View style={{flex: 2}}>
                    <TouchableOpacity
                      onPress={() => this.onPressone()}
                      style={{
                        margin: 10,
                        flex: 1,
                        height: 45,
                        width: 80,
                        backgroundColor: buttonBgone,
                        borderColor: 'blue',
                        borderWidth: 1,
                      }}>
                      <Text
                        style={{
                          color: textcolorone,
                          fontSize: 16,
                          textAlign: 'center',
                          marginTop: 10,
                        }}>
                        {textValueone}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 2}}>
                  <Text
                    style={{fontSize: 18, fontWeight: 'bold', marginTop: 10}}>
                    Comics
                  </Text>
                </View>
                <View>
                  <View style={{flex: 2}}>
                    <TouchableOpacity
                      onPress={() => this.onPresstwo()}
                      style={{
                        margin: 10,
                        flex: 1,
                        height: 45,
                        width: 80,
                        backgroundColor: buttonBgtwo,
                        borderColor: 'blue',
                        borderWidth: 1,
                      }}>
                      <Text
                        style={{
                          color: textcolortwo,
                          fontSize: 16,
                          textAlign: 'center',
                          marginTop: 10,
                        }}>
                        {textValuetwo}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 2}}>
                  <Text
                    style={{fontSize: 18, fontWeight: 'bold', marginTop: 10}}>
                    Fiction
                  </Text>
                </View>
                <View>
                  <View style={{flex: 2}}>
                    <TouchableOpacity
                      onPress={() => this.onPressthree()}
                      style={{
                        margin: 10,
                        flex: 1,
                        height: 45,
                        width: 80,
                        backgroundColor: buttonBgthree,
                        borderColor: 'blue',
                        borderWidth: 1,
                      }}>
                      <Text
                        style={{
                          color: textcolorthree,
                          fontSize: 16,
                          textAlign: 'center',
                          marginTop: 10,
                        }}>
                        {textValuethree}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 2}}>
                  <Text
                    style={{fontSize: 18, fontWeight: 'bold', marginTop: 10}}>
                    Film
                  </Text>
                </View>
                <View>
                  <View style={{flex: 2}}>
                    <TouchableOpacity
                      onPress={() => this.onPressfour()}
                      style={{
                        margin: 10,
                        flex: 1,
                        height: 45,
                        width: 80,
                        backgroundColor: buttonBgfour,
                        borderColor: 'blue',
                        borderWidth: 1,
                      }}>
                      <Text
                        style={{
                          color: textcolorfour,
                          fontSize: 16,
                          textAlign: 'center',
                          marginTop: 10,
                        }}>
                        {textValuefour}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 2}}>
                  <Text
                    style={{fontSize: 18, fontWeight: 'bold', marginTop: 10}}>
                    Gaming
                  </Text>
                </View>
                <View>
                  <View style={{flex: 2}}>
                    <TouchableOpacity
                      onPress={() => this.onPressfive()}
                      style={{
                        margin: 10,
                        flex: 1,
                        height: 45,
                        width: 80,
                        backgroundColor: buttonBgfive,
                        borderColor: 'blue',
                        borderWidth: 1,
                      }}>
                      <Text
                        style={{
                          color: textcolorfive,
                          fontSize: 16,
                          textAlign: 'center',
                          marginTop: 10,
                        }}>
                        {textValuefive}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Card>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default Tab1;
