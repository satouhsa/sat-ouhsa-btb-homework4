import React from 'react'
import { View, Text } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Tab1 from './Tab1';
import Tab2 from './Tab2';
import Tab3 from './Tab3';
const MaterialTopTabs = createMaterialTopTabNavigator();
const Interest = () => {
    return (
        <MaterialTopTabs.Navigator>
        <MaterialTopTabs.Screen
          name="Topic"
          component={Tab1}
          
        />
        <MaterialTopTabs.Screen name="Peopel" component={Tab2} />
        <MaterialTopTabs.Screen name="Pulication" component={Tab3} />
      </MaterialTopTabs.Navigator>
    )
}

export default Interest
