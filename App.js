/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */


import {
  NavigationContainer,
  DarkTheme,
  DrawerActions
} from '@react-navigation/native';

import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import {  AppearanceProvider } from 'react-native-appearance';

import React from 'react'

import { Image,TouchableOpacity } from 'react-native';
import Homepage from './components/Homepage';
import Login from './components/Login';
import Show from './components/Show';
import Interest from './components/Interest';
import Strories from './components/Strories';
import { Button } from 'react-native-paper';
import Profile from './components/Profile';
import NewStrory from './components/NewStrory';
import Become from './components/Become';
import Logout from './components/Logout';
import {Linking} from 'react-native';


const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
// const MaterialBottomTabs = createMaterialBottomTabNavigator();
const MaterialTopTabs = createMaterialTopTabNavigator();

const App = () => {


 

  createHomeStack = () =>
    <Stack.Navigator>
      <Stack.Screen
        name="Feed"
        children={this.createDrawer}
        options={({ navigation }) => ({
          title: "React Navigation",
      
          headerLeft: () =>
            
            <TouchableOpacity  onPress={()=>navigation.dispatch(DrawerActions.toggleDrawer())}>

            <Image source={{uri:'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Hamburger_icon.svg/1024px-Hamburger_icon.svg.png'}}    
            style={{width: 30, height: 30,marginLeft:10}}
           
            >

            </Image>

            </TouchableOpacity>,
            headerRight:()=>
                <TouchableOpacity onPress={()=>{Linking.openURL('tel:8777111223')}}> 

                  <Image source={{uri:'https://w7.pngwing.com/pngs/231/596/png-transparent-computer-icons-mobile-phones-telephone-call-text-telephone-call-phone-icon-thumbnail.png'}}    
                  style={{width: 30, height: 30,marginRight:20}}
                
                  >

            </Image>

              </TouchableOpacity>
           
        })
        }
      />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="HomeScreen" component={Homepage} 
        
      />
     
     
      <Stack.Screen
        name="Show"
        component={Show}
        options={{
          title: "Show"
        }}
      />
    
    </Stack.Navigator>

  createDrawer = () =>
    <Drawer.Navigator>
      <Drawer.Screen name="Login" component={Login}/>
      <Drawer.Screen name="HomeScreen" component={Homepage}/>
      <Drawer.Screen name="Interest" component={Interest} />
      <Drawer.Screen name="Strories" component={Strories} />
      <Drawer.Screen name="Profile" component={Profile}/>
      <Drawer.Screen name="New Strory" component={NewStrory}/>
      <Drawer.Screen name="Become a member " component={Become}/>
    
         <Drawer.Screen name="Logout" component={Logout} />
    
     
      {/* <Drawer.Screen name="Favorites" component={Favorites} />
      <Drawer.Screen name="Settings" component={Settings} /> */}
    </Drawer.Navigator>

  createTopTabs = (props) => {
    return <MaterialTopTabs.Navigator>
      <MaterialTopTabs.Screen
        name="Tab1"
        component={Tab1}
        options={{ title: props.route.params.name }}
      />
      <MaterialTopTabs.Screen name="Tab2" component={Tab2} />
      <MaterialTopTabs.Screen name="Tab3" component={Tab3} />
    </MaterialTopTabs.Navigator>
  }

  return (
    <AppearanceProvider>
    <NavigationContainer>
      {this.createHomeStack()}
     
    </NavigationContainer>
  </AppearanceProvider>
  )
}

export default App;
